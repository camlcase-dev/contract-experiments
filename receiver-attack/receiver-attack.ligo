type storage is unit;

function main (const params : (address * address); const storage : storage) : (list(operation) * storage) is
  block {
    var op_list: list(operation) := nil;

    const receive: contract(address) = get_entrypoint("%receive", params.0);
    const op1: operation = transaction (params.1, amount, receive);

    const set_balance: contract(unit) = get_entrypoint("%set", params.0);
    const op2: operation = transaction(unit, 0mutez, set_balance);
    
    op_list := list op1; op2; end;
  } with (op_list, storage);

# receiver attack

Consider two entrypoints:

- receive: forwards funds to a provided address
- set_: set storage as the value from BALANCE operation

An invariant of receiver is that storage is always zero.

- All XTZ sent to receive is sent to another address (cannot be the receiver contract).
- Set cannot receive tezos.

This invariant can be broken with receive-attack.ligo.

It calls receive with some XTZ, then it calls set_. Because BALANCE does not change when
set_ is called but AMOUNT has, it is able to break our expectations of what receiver.ligo
does.

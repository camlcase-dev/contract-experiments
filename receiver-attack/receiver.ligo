type entrypoint is
| Receive of address
| Set of unit

type storage is tez;

type return is (list(operation) * storage);

// send XTZ to an address via this contract
function receive(const to_ : address; const storage : storage): return is
  block {
    var op_list: list(operation) := nil;
  
    if (to_ =/= self_address) then {
      const to_contract: option(contract(unit)) = Tezos.get_contract_opt(to_);


      case (to_contract) of
        | None -> failwith("can't send xtz")
        | Some(to_contract) -> {
          const op1: operation = transaction(unit, amount, to_contract);
          op_list := list op1; end;      
        }
      end;
    } else {
      failwith("can't send xtz to this contract")
    }
  } with (op_list, storage);

function set_(var storage : storage): return is
  block {
    const op_list: list(operation) = nil;
  
    if (amount > 0mutez) then {
      failwith("Do not send tezos.");
    } else {
      storage := balance;
    };

  } with (op_list, storage);

// invariant of this contract is that storage is always zero

function main (const entrypoint : entrypoint ; const storage : storage) : (list(operation) * storage) is
  (case entrypoint of
  | Receive(xs) -> receive(xs, storage)
  | Set(xs)     -> set_(storage)
  end);

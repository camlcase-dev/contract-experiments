#!/bin/bash
# chmod u+x compile.sh

# last compiled with
# ligo --version
# Commit SHA1: 77eb1335a1fecd5729d5f8c79830528a07964f7a
# Commit Date: 2020-06-22 22:58:59 +0000

# this script depends on:
# - dos2unix

ligo compile-contract ./receiver.ligo main | dos2unix > contracts/receiver.tz

ligo compile-contract ./receiver-attack.ligo main | dos2unix > contracts/receiver-attack.tz

# The invariant attack

The storage of contracts/receiver.tz should not change according to the code, but 
the contract contract/receiver-attack.tz does in fact set the value to something 
it did not expect.

# run the experiment

```
tezos-client originate contract receiver transferring 0 from alice running ./contracts/receiver.tz --init '0' --burn-cap 2 --force

tezos-client originate contract receiver-attack transferring 0 from alice running ./contracts/receiver-attack.tz --init 'Unit' --burn-cap 2 --force
```

```bash
tezos-client list known contracts

receiver-attack: KT1Ng4S9bxtAWNMP2BCi2gGpE4yoU6zP6Sg5
receiver: KT19LxQTuZcfVydXqwYFpBS2LzEEVVFXdBGt
bob: tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z
alice: tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV
```

# receive test

```
tezos-client transfer 1 from alice to receiver --entrypoint 'receive' --arg '"tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z"' --burn-cap 1

tezos-client get contract storage for receiver
```

# set test should always make storage zero

```
tezos-client transfer 0 from alice to receiver --entrypoint 'set' --arg 'Unit' --burn-cap 1
```

# attack

```
tezos-client transfer 1 from alice to receiver-attack --arg 'Pair "KT19LxQTuZcfVydXqwYFpBS2LzEEVVFXdBGt" "tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z"' --burn-cap 1
```

The invariant has changed in an unexpected way.

```
tezos-client get contract storage for receiver
```


# call order

This first experiment shows that even though the contract calls operations, 
a, b, c, d in this order, the actual order is a, d, b, c.

There are two compiled .tz contracts in ./contracts.

- log.tz: append received string to storage with ":" as the prefix.
- call-order.tz: perform an experiment that calls log.tz multiple times and 
  stores a value in its storage for each call

# originate log
```bash
tezos-client originate contract log transferring 0 from alice running ./contracts/log.tz --init '""' --burn-cap 2 --force
```

# appendLog entrypoint
```bash
tezos-client transfer 0 from alice to log --entrypoint 'appendLog' --arg '"hello"' --burn-cap 1

tezos-client transfer 0 from alice to log --entrypoint 'appendLog' --arg '"goodbye"' --burn-cap 1
```

# clearLog entrypoint
```bash
tezos-client transfer 0 from alice to log --entrypoint 'clearLog' --arg 'Unit' --burn-cap 1

tezos-client get contract storage for log
```

# originate call-order

```bash
tezos-client originate contract call-order transferring 0 from alice running ./contracts/call-order.tz --init 'Unit' --burn-cap 2 --force
```

# get log contract address
```bash
tezos-client list known contracts
call-order: KT1Rh5s9SR81kjvjR4Uw4PHqyS4Sx9JAzgzu
log: KT1VcuxbAdwJnHF1D49cUAGUCnMTLaqwCTU1
```

# run experiment from call-order

```bash
tezos-client transfer 0 from alice to call-order --entrypoint 'callOrderExperiment' --arg '"KT1VcuxbAdwJnHF1D49cUAGUCnMTLaqwCTU1"' --burn-cap 1
```

Check the storage of log to confirm the result.

```bash
tezos-client get contract storage for log
```


# Operation queue from callOrderExperiment

[Michelson Inter-transaction semantics](http://tezos.gitlab.io/whitedoc/michelson.html#inter-transaction-semantics)

| executing       | emissions     | resulting queue     |
| ------------- | ---------- | ----------- |
|  log.appendLog("a") | self.appendRemoteLog(log_address), log.appendLog("d") | self.appendRemoteLog(log_address), log.appendLog("d") |
|  self.appendRemoteLog(log_address) | log.appendLog("b"), self.appendRemoteLog("c", log_address) | log.appendLog("d"), log.appendLog("b"), self.appendRemoteLog("c", log_address) |
| log.appendLog("d") |  | log.appendLog("b"), self.appendRemoteLog("c", log_address) |
| log.appendLog("b") |  | self.appendRemoteLog("c", log_address) |
| self.appendRemoteLog(“c”, log_address) | log.appendLog("c") | log.appendLog("c") |
| log.appendLog("c") |  | |

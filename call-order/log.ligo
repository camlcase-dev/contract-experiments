type entrypoint is
| AppendLog of string
| ClearLog of unit

type storage is record
  log: string;
end

const empty_ops : list(operation) = list end;

type return is (list(operation) * storage);

function append_log(const entry  : string;
                    var  storage : storage):
                 return is
  block {
    storage.log := storage.log ^ ":" ^ entry;
  } with (empty_ops, storage);

function clear_log(var  storage : storage):
                 return is
  block {
    storage.log := "";
  } with (empty_ops, storage);

function main (const entrypoint : entrypoint ; const storage : storage) : (list(operation) * storage) is
  (case entrypoint of
  | AppendLog(xs) -> append_log(xs, storage)
  | ClearLog(xs) -> clear_log(storage)
  end);


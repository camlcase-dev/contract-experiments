type entrypoint is
| AppendRemoteLog of (address * string)
| AppendRemoteLogIntermediate of address
| CallOrderExperiment of address

// the transfer entrypoint of the FA1.2 contract
type log_contract_append_log is string;

type storage is unit;

type return is (list(operation) * storage);

function append_remote_log(const log_address: address;
                           const message: string): return is
  block {
    var op_list: list(operation) := nil;
  
    const log_contract: contract(log_contract_append_log) = get_entrypoint("%appendLog", log_address);
    const op1: operation = transaction(message, 0mutez, log_contract);
    op_list := list op1; end;

  } with (op_list, Unit);

function append_remote_log_intermediate(const log_address: address): return is
  block {
    var op_list: list(operation) := nil;

    // b call
    const log_contract: contract(log_contract_append_log) = get_entrypoint("%appendLog", log_address);
    const op1: operation = transaction("b", 0mutez, log_contract);
    
    // c call
    const self_contract : contract(address * string) = Tezos.self("%appendRemoteLog") ;
    const op2: operation = transaction((log_address, "c"), 0mutez, self_contract);

    op_list := list op1; op2; end;

  } with (op_list, Unit);

function call_order_experiment(const log_address: address): return is
  block {
    var op_list: list(operation) := nil;

    // a call
    const log_contract: contract(log_contract_append_log) = get_entrypoint("%appendLog", log_address);
    const op1: operation = transaction("a", 0mutez, log_contract);    

    // b and c call
    const self_contract : contract(address) = Tezos.self("%appendRemoteLogIntermediate");
    const op2: operation = transaction(log_address, 0mutez, self_contract);
    
    // d call
    const op3: operation = transaction("d", 0mutez, log_contract);
    op_list := list op1; op2; op3; end;

  } with (op_list, Unit);

function main (const entrypoint : entrypoint ; const storage : storage) : (list(operation) * storage) is
  (case entrypoint of
  | AppendRemoteLog(xs) -> append_remote_log(xs.0, xs.1)
  | AppendRemoteLogIntermediate(xs) -> append_remote_log_intermediate(xs)
  | CallOrderExperiment(xs) -> call_order_experiment(xs)
  end);
